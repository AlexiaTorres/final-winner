<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WinnerController extends Controller
{
    public function getWinner(Request $request)
    {
        $names = $request->namesList;
        $data = [
            "status" => "200",
            "winner" => $names[array_rand(array_filter($names))]
        ];

       return response()->json($data);
    }
}