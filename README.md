# Final Winner

## About Final Winner

This is an application for get a winner between finalist profiles for instagram giveaways

## Run

- clone the repository and go to final winner directory
- copy .env.example to .env
- npm install
- composer install
- php artisan serve
- open http://localhost:8000
