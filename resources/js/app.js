import VueRouter from 'vue-router';
import router from './router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './App.vue';

require('./bootstrap');

window.Vue = require('vue').default;

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.component('home-component', require('./components/HomeComponent.vue').default);
Vue.component('content-component', require('./components/ContentComponent.vue').default);
Vue.component('footer-component', require('./components/FooterComponent.vue').default);
Vue.component('winner-component', require('./components/WinnerComponent.vue').default);

const app = new Vue({
    el: '#app',
    components: {App},
    router
});
