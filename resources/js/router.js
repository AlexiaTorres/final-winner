import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeComponent from "./components/HomeComponent";

Vue.use(VueRouter);

// Routes
const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'is-active',
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeComponent
        },
    ],
});

export default router