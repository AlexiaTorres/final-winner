<!DOCTYPE html>

<html>
<head>
    @include('includes.head')
    @stack('styles')
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div id="app">
    <app></app>
</div>
@include('includes.footer')
@stack('scripts')
</body>
</html>